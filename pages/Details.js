import { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import DestinationsContext from '../context/DestinationsContext';
import { DETAILS, CITIES } from '../context/types/DestinationTypes';
import Banner from '../components/Banner'; 
import DestinationInfo from '../components/DestinationInfo';
/* import Cities from '../components/cities/Cities';
import Footer from '../components/footer/Footer'; */
const Details = () => {
	const { destinationsData, dispatch } = useContext(DestinationsContext);
	const { details, filteredCities } = destinationsData;
	const { id } = useParams();
	useEffect(() => {
		dispatch({ type: DETAILS, payload: id });
		dispatch({ type: CITIES, payload: id });
		window.scrollTo(0, 0);
	}, [id]);
	return (
		<>
			
			<Banner heading={details.name} image={details.bigImage}></Banner>
			<DestinationInfo details={details} />
		
			{/* <Footer /> */}
		</>
	);
};
export default Details;
