const reviews = [
	{
		id: 1,
		name: 'ali',
		comment:
			"That's great travel agency so far I see with great team and user friendly",
		stars: 4,
		image: '/assets/reviewUsers/user1.jpg',
	},
	{
		id: 2,
		name: 'alex',
		comment:
			'I suggest travel friends to everyone because I love travel friends',
		stars: 5,
		image: '/assets/reviewUsers/user2.jpg',
	},
	{
		id: 3,
		name: 'andrison',
		comment: 'One my favorite travel website',
		stars: 3,
		image: '/assets/reviewUsers/user3.jpg',
	},
	{
		id: 4,
		name: 'brad hussy',
		comment: 'I love the hotels which are associated with travel friends.',
		stars: 5,
		image: '/assets/reviewUsers/user4.jpg',
	},
	{
		id: 5,
		name: 'rahul gandi',
		comment:
			'Me and my wife both are big fans of the travel friends website because they have everything that you want, 5-star rooms.',
		stars: 5,
		image: '/assets/reviewUsers/user6.jpg',
	},
	{
		id: 6,
		name: 'shakil khan',
		comment:
			"I love the facilities especially when I reached to USA, travel friend's car was present there for me.",
		stars: 2,
		image: '/assets/reviewUsers/user5.jpg',
	}
];
export default reviews;
