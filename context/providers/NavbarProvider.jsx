import { useReducer } from 'react';
import NavbarContext from '../NavbarContext';
import NavbarReducer from '../reducers/NavbarReducer';
const NavbarProvider = (props) => {
	const [state, dispatch] = useReducer(NavbarReducer, false);
	return (
		<NavbarContext.Provider value={{ state, dispatch }}>
			{props.children}
		</NavbarContext.Provider>
	);
};
export default NavbarProvider;