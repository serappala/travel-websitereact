import { NAVBAR_TOGGLE } from '../types/NavbarTypes';
const NavbarReducer = (state, action) => {
	if (action.type === NAVBAR_TOGGLE) {
		return !state;
	} else {
		return state;
	}
};
export default NavbarReducer;