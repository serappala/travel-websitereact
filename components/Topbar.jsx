import Link  from 'next/link';
import { BsLinkedin } from "react-icons/bs";
import { FaGithubSquare } from "react-icons/fa";
import { AiOutlineMail } from "react-icons/ai";
import { AiOutlinePhone } from "react-icons/ai";

function Topbar() {



  return (
<>

<div className="topbar">
      <div className="container">
        <div className="logo">
          <Link href="/" className="li">
            <div><i>Yardım</i></div>
          </Link>
        </div>

        <ul className="top-social-menu">
          <Link href="/" className="li">
            <BsLinkedin className="topbaricon" />
          </Link>

          <Link href="/" className="li">
            <FaGithubSquare className="topbaricon" />
          </Link>
        </ul>

        <div className="contact-area ">
          <Link className="btnn m-2"  href="/create">
         +90123456789
            <span className='ml-2'>
              <AiOutlinePhone />
            </span>
          </Link>

          <Link className="btnn ml-2" href="/contact">
          example@gmail.com
            <span className='ml-5'>
              <AiOutlineMail />
            </span>
          </Link>
        </div>
      

       
      </div>
    </div>
    </>
  )
}

export default Topbar