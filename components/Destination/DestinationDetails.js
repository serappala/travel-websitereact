import { useRouter } from "next/router";

const DestinationDetails = ({ title, text }) => {
	const router= useRouter()
	const {url} = router.query
	return (
		<div className='destinationInfo__details'>
				<div className='destinationInfo__details__head animation'>{url}</div>
			<div className='destinationInfo__details__head animation'>{title}</div>
			<div className='destinationInfo__details__text animation'>{text}</div>
		</div>
	);
};
export default DestinationDetails;
