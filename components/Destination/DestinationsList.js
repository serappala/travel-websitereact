import { LazyLoadImage } from 'react-lazy-load-image-component';
import { GiPositionMarker } from 'react-icons/gi';
import Link  from 'next/link';
const DestinationsList = ({ destination }) => {
	return (
		<div className='col-xl-3 col-md-12 p-15 mb-4'>
			<div className='destinations__card animation'>
				<div className='destinations__card__img'>
					<LazyLoadImage src={destination.image} alt={destination.image} />
				</div>
				<div className='destinations__card__layer'>
					<div className='destinations__card__layer__content'>
						<GiPositionMarker size={20} />
						<div className='destinations__card__layer__content__country'>
							{destination.name}
						</div>
					</div>
				</div>
				<div className='destinations__card__info'>
					<div className='destinations__card__info__text'>
						<Link className='btn-white' href={`/details/${destination.id}`}>
							Explore
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};
export default DestinationsList;
