import { useContext } from 'react';
import NavbarContext from '../context/NavbarContext';
import { NAVBAR_TOGGLE } from '../context/types/NavbarTypes';
const Toggle = () => {
	const { state, dispatch } = useContext(NavbarContext);
	return (
		<div
			className={state ? 'toggle-close' : 'toggle-open'}
			onClick={() => dispatch({ type: NAVBAR_TOGGLE })}>
			<span></span>
			<span></span>
			<span></span>
		</div>
	);
};
export default Toggle;