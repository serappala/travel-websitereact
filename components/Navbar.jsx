import Link from "next/link";
import { useContext } from "react";

import NavbarContext from "../context/NavbarContext";

function Navbar() {
  const { state, dispatch } = useContext(NavbarContext);
  console.log("navbar state: ", state);

  return (
    <>
      {state ? <div className="navbarLayer"></div> : ""}

      <div className={state ? "navbar navbar--open" : "navbar navbar--close"}>
        <div className="navbar__content">
          <li>
            <Link href="/">Home</Link>
          </li>
          <li>
            <Link href="/about">About</Link>
          </li>
          <li>
            <Link href="/contact">Contact</Link>
          </li>
        </div>
      </div>

      <div class="container">
        <div class="nav-header">
          <nav class="navbarDesktop">
            <div className="logo">
              <Link href="/" className="li">
                <div>Travel</div>
              </Link>
            </div>

            <ul class="navbar-list">
            <li>  <Link  className="a" href="/">Home</Link></li>

            <li> <Link className="a" href="/about">
                About
                <ul class="dropdown">
                <li>   <Link  className="a"href="/about">About 1</Link></li>
                <li> <Link className="a" href="/about">About 2</Link></li>
                </ul>
              </Link></li>

              <li>  <Link className="a" href="/contact">Contact</Link></li>
            </ul>

            <div class="search-area">
              <form>
                <input type="search" placeholder="Search" />
                <button type="submit" class="btn">
                  <i class="fa fa-search"></i>
                </button>
              </form>
            </div>

            <label for="chek" class="close-menu"></label>
          </nav>
        </div>
      </div>
    </>
  );
}

export default Navbar;
