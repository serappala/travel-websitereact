import Head from "next/head";
//
import Banner from "./Banner";
import Destinations from "./Destination/Destinations";
import Review from "./reviews/Reviews";
import Gallery from "./gallery/GalleryComponent";
import Cities from "./cities/Cities";
import Topbar from "./Topbar";
import Navbar from "./Navbar";
import Toggle from "./Toggle";
import Footer from "./footer/Footer";
import Services from "../components/services/Services";
import Bodym from "../components/Bodym";
//
import NavbarProvider from "../context/providers/NavbarProvider";
import ModalProvider from "../context/providers/ModalProvider";
import DestinationsProvider from "../context/providers/DestinationsProvider";
import SharedProvider from "../context/providers/SharedProvider";
import GalleryProvider from "../context/providers/GalleryProvider";
import AnimationsProvider from "../context/providers/AnimationsProvider";

function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Travel Website</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta charset="utf-8" />

        <link rel="apple-touch-icon" sizes="180x180" href="assets/favicons/" />

        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="assets/favicons//favicon-32x32.png"
        />

        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="assets/favicons//favicon-16x16.png"
        />

        <link rel="manifest" href="assets/favicons//site.webmanifest" />
      </Head>

      <ModalProvider>
        <NavbarProvider>
          <GalleryProvider>
            <DestinationsProvider>
              <SharedProvider>
                <AnimationsProvider>

                  <Toggle />
                  <Topbar />
                  <Navbar />
                  <Banner />
                  <main> {children}</main>
                  <Destinations />
                 {/*  <Banner />
                  
                
                  <Bodym />
                  <Review />
                  <Gallery />
                  <Services /> */}
                  <Footer />
                </AnimationsProvider>
              </SharedProvider>
            </DestinationsProvider>
          </GalleryProvider>
        </NavbarProvider>
      </ModalProvider>
    </>
  );
}

export default Layout;
