import React, { useEffect, useRef } from "react";
import lottie from "lottie-web";

function Bodym() {
  const container = useRef(null);

  useEffect(() => {
    lottie.loadAnimation({
      container: container.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: require("../bodymovin/95577-graphics-day.json"),
    });
  }, []);

  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col-12"> <div className="container" ref={container}></div></div>
        </div>
      </div>

     
    </div>
  );
}

export default Bodym;
