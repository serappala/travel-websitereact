import { useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Image from 'next/image'

const Banner = ({ heading, paragraph, children, image }) => {
  const [state] = useState({
    video: "/assets/videos/header.mp4",
    poster: "/assets/images/screen.png",
    logo: '/assets/images/logo.png',
    heading: 'We are travel friends',
    paragraph:
      'Come and join us we travel the most famous and beautiful places in the world',

  });

  return (
    <div className="banner">

      <div className='container pr'>
        <div className='banner__logo'>
          <Image src={state.logo}  alt="banner"
      width={500}
      height={500} />
        </div>
      </div>


      <div className='banner__video'>
				{image ? (
					<LazyLoadImage src={image} alt={image} />
				) : (
					<video
						src={state.video}
						autoPlay
						loop
						muted
						poster={state.poster}></video>
				)}

			</div>
      <div className="banner__contents">
        <div className="container">
          <div className="banner__contents__text">
            <div className="banner__contents__text__child">
              <h1 className="banner__contents__text__child__h1">
                {state.heading}
              </h1>
              <p className="banner__contents__text__child__p mt-4 ">

                {state.paragraph}
              </p>
              <div className="banner__contents__text__child__link mt-4">
                  {state.children} 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;